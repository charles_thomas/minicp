# README #

* use IntelliJ IDE and import directly the project or do a command line compilation using SBT www.scala-sbt.org (commands compile, tests, run)
* website with technical documentation, exercises etc  https://www.info.ucl.ac.be/~pschaus/minicp

[ ![Codeship Status for pschaus/minicp](https://app.codeship.com/projects/c5b42a30-bb10-0134-c1e5-0a15df6d3688/status?branch=master)](https://app.codeship.com/projects/195547)

[![codecov](https://codecov.io/bb/pschaus/minicp/branch/master/graph/badge.svg?token=zAUOtKaB64)](https://codecov.io/bb/pschaus/minicp)
